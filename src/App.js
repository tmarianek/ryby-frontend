import './App.css';
import {createContext, useState} from "react";
import Views from "./component/Views";

// TODO: UserContext bych dal zvlast do souboru, klidně i s tím useState a providerem.
export const UserContext = createContext();

function App() {
    const [user, setUser] = useState({loggedIn: false});

    return (
        <UserContext.Provider value={{user, setUser}}>
            <Views/>
        </UserContext.Provider>);
}

export default App;
