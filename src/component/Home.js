import {useContext} from "react";
import {UserContext} from "../App";
import {Alert, Button, Form, FormGroup} from "react-bootstrap";

function Home() {
    const {user, setUser} = useContext(UserContext);
    return (<div> {`Home: ${user.loggedIn} token: ${user.token}`}
        <Form.Group controlId="dob">
            <Form.Label>Select Date</Form.Label>
            <Form.Control type="date" name="dob" placeholder="Date of Birth" />
        </Form.Group>
    </div>)
}

export default Home;
