import {Route, Routes} from "react-router-dom";
import ProtectedRoutes from "./ProtectedRoutes";
import Login from "./user/Login";
import Home from "./Home";
import {useContext} from "react";
import {UserContext} from "../App";
import MainMenu from "./MainMenu";
import AttendanceList from "./attendance/AttendanceList";
import PermissionList from "./permission/PermissionList";
import Registration from "./user/Registration";
import PermissionAdd from "./permission/PermissionAdd";
import PermissionDetail from "./permission/PermissionDetail";
import AttendanceAdd from "./attendance/AttendanceAdd";


function Views() {
    const {user} = useContext(UserContext);

    return (
        <div>
            {user.loggedIn && <MainMenu/>}
            <Routes>
                <Route path="/" element={<Login/>}/>
                <Route path="/registration" element={<Registration/>}/>
                <Route element={<ProtectedRoutes/>}>
                    <Route path="/home" element={<Home/>}/>
                    <Route path="/attendance" element={<AttendanceList/>}/>
                    <Route path="/attendance-add/:uuid" element={<AttendanceAdd/>}/>
                    <Route path="/permission" element={<PermissionList/>}/>
                    <Route path="/permission-add" element={<PermissionAdd/>}/>
                    <Route path="/permission/:uuid" element={<PermissionDetail/>}/>
                </Route>
            </Routes>
        </div>
    );
}

export default Views;
