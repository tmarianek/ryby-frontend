const GET_ALL = "http://localhost:8080/api/v1/permit"
class  PermissionAPI{

    async getAll(token){
        const response = await fetch(GET_ALL , {
            method: 'get',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Access-Control-Request-Headers': 'session_token',
                'session_token' : token
            }
        });

        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(response.status);
        }
    }
}

export default new PermissionAPI();
