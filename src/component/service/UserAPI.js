const LOGIN_REST_API = 'http://localhost:8080/api/v1/session/login';
const CHECK_TOKEN = 'http://localhost:8080/api/v1/session'

class UserAPI {

    async login(data = {}) {
        const response = await fetch(LOGIN_REST_API, {
            method: 'post',
            headers: {
                Accept: 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(data)
        });

        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(response.status);
        }
    }

    async checkToken (token) {
        const response = await fetch(CHECK_TOKEN + "/" + token, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json;charset=UTF-8',
                'session_token' : token
            }
        });

        if (response.ok) {
            return await response.json();
        } else {
            throw new Error(response.status);
        }
    }
}

export default new UserAPI();
