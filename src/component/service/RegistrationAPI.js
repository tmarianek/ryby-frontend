const BOOKS_REST_API = 'http://localhost:8080/api/v1/registration';

class RegistrationAPI {
    async registration(data = {}) {
        const response = await fetch(BOOKS_REST_API, {
            method: 'post',
            headers: {
                Accept: 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(data)
        });
        if (response.ok) {
            return;
        } else {
            throw new Error(response.status);
        }
    }

}

export default new RegistrationAPI();
