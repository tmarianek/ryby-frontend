import React, {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import RegistrationAPI from "../service/RegistrationAPI";
import {Alert, Button, Form, FormGroup} from "react-bootstrap";

function Registration() {
    const navigate = useNavigate();
    const [inputs, setInputs] = useState({});
    const [error, setError] = useState();
    const [formErrors, setFormErrors] = useState({});

    const setField = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value
        });

        if (!!formErrors[field]) setFormErrors({
            ...formErrors,
            [field]: null
        })
    }

    const errorCodes = [{"status": "400", "message": "Chyba validace"},
        {"status": "409", "message": "Uzivatel jiz existuje"}];

    const findFormErrors = () => {
        const {email, name, password, passwordAgain} = inputs
        const newErrors = {}

        if (!email || email === '') newErrors.email = 'Email je povinny'

        if (!name || name === '') newErrors.name = 'Jmeno je povinne'

        if (!password || password === '') newErrors.password = 'Heslo je povinne'
        if (!passwordAgain || passwordAgain === '') newErrors.passwordAgain = 'Potvrzeni hesla je povinne'

        if (!(password === passwordAgain)) newErrors.passwordAgain = "Potvrzeni heslane ni stejne jako heslo"

        return newErrors
    }

    const handleRegistration = (event) => {
        event.preventDefault();

        const newError = findFormErrors();

        if (Object.keys(newError).length > 0) {
            setFormErrors(newError);
        } else {
            setFormErrors({});
            const data = {
                name: inputs.name,
                email: inputs.email,
                password: inputs.password
            }

            const response = RegistrationAPI.registration(data);
            response
                .then(res => navigate('/'))
                .catch(err => {
                    const errorCode = errorCodes.find(e => e.status === err.message);
                    setError(errorCode.message);
                });
        }
    }

    return (
        <div className="login-form">
            {error && (<Alert variant="danger">{error}</Alert>)}
            <Form onSubmit={handleRegistration} style={{width: '340px'}}>
                <FormGroup>
                    <Form.Label>Email</Form.Label>
                    <Form.Control type='email'
                                  onChange={e => setField('email', e.target.value)}
                                  isInvalid={!!formErrors.email}/>
                    <Form.Control.Feedback type='invalid'>
                        {formErrors.email}
                    </Form.Control.Feedback>
                </FormGroup>
                <FormGroup>
                    <Form.Label>Jmeno</Form.Label>
                    <Form.Control type='text'
                                  onChange={e => setField('name', e.target.value)}
                                  isInvalid={!!formErrors.name}/>
                    <Form.Control.Feedback type='invalid'>
                        {formErrors.name}
                    </Form.Control.Feedback>
                </FormGroup>
                <FormGroup>
                    <Form.Label>Heslo</Form.Label>
                    <Form.Control type='password'
                                  onChange={e => setField('password', e.target.value)}
                                  isInvalid={!!formErrors.password}/>
                    <Form.Control.Feedback type='invalid'>
                        {formErrors.password}
                    </Form.Control.Feedback>
                </FormGroup>
                <FormGroup>
                    <Form.Label>Potvrzeni hesla</Form.Label>
                    <Form.Control type='password'
                                  onChange={e => setField('passwordAgain', e.target.value)}
                                  isInvalid={!!formErrors.passwordAgain}/>
                    <Form.Control.Feedback type='invalid'>
                        {formErrors.passwordAgain}
                    </Form.Control.Feedback>
                </FormGroup>
                <Button type="submit" className="btn btn-primary btn-dark">Registrace</Button>
            </Form>
        </div>
    )
}

export default Registration
