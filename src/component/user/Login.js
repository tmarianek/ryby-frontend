import React, {useContext, useState} from 'react';
import './Login.css'
import {UserContext} from "../../App";
import {Link, useNavigate} from "react-router-dom";
import {useLocation} from "react-router";
import {Alert, Button, Form, FormGroup} from "react-bootstrap";
import UserAPI from "../service/UserAPI";

function Login() {
    const {user, setUser} = useContext(UserContext);
    const [inputs, setInputs] = useState({});
    const [error, setError] = useState();
    const [formErrors, setFormErrors] = useState({});


    const navigate = useNavigate();
    const location = useLocation();

    const setField = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value
        });

        if (!!formErrors[field]) setFormErrors({
            ...formErrors,
            [field]: null
        })

    }
    const errorCodes = [{"status": "404", "message": "Uzivatelske jmeno neexistuje"},
        {"status": "409", "message": "Chybne heslo"}];

    const findFormErrors = () => {
        const {email, password} = inputs
        const newErrors = {}

        if (!email || email === '') newErrors.email = 'Email je povinny'

        if (!password || password === '') newErrors.password = 'Heslo je povinne'
        return newErrors
    }

    const handleLogin = async (event) => {
        event.preventDefault();

        const newError = findFormErrors();

        if (Object.keys(newError).length > 0) {
            setFormErrors(newError);
        } else {
            setFormErrors({});
            const data = {
                name: inputs.email,
                pass: inputs.password
            }

            const response = UserAPI.login(data);
            response
                .then(res => {
                    console.log(res)
                    // TODO: nerozjížděl jsem to ale pokud si ukládáš token jen do state, tak refresh stránky odhlásí uživatele
                    //   Bezne se token dava i do local nebo session storage, odkud se po refreshi vytahne.
                    setUser({loggedIn: true, token : res.token});

                    if (location.state?.from) {
                        navigate(location.state.from);
                    } else {
                        navigate('/permission');
                    }
                })
                .catch(err => {
                    const errorCode = errorCodes.find(e => e.status === err.message);
                    setError(errorCode.message);
                });
        }

    }

    return (
        <div className="login-form">
            {error && (<Alert variant="danger">{error}</Alert>)}
            <Form onSubmit={handleLogin} style={{width: '340px'}}>
                <FormGroup>
                    <Form.Label>Email</Form.Label>
                    <Form.Control type='email'
                                  onChange={e => setField('email', e.target.value)}
                                  isInvalid={!!formErrors.email}/>
                    <Form.Control.Feedback type='invalid'>
                        {formErrors.email}
                    </Form.Control.Feedback>
                </FormGroup>
                <FormGroup>
                    <Form.Label>Heslo</Form.Label>
                    <Form.Control type='password'
                                  onChange={e => setField('password', e.target.value)}
                                  isInvalid={!!formErrors.password}/>
                    <Form.Control.Feedback type='invalid'>
                        {formErrors.password}
                    </Form.Control.Feedback>
                </FormGroup>
                <Button type="submit" className="btn btn-primary btn-dark">Prihlasit se</Button> <br/>
                <Link to={'/registration'}>registrace</Link>
            </Form>
        </div>
    )
}

export default Login;
