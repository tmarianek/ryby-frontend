import {Alert, Button, Container, Form, FormGroup} from "react-bootstrap";
import React, {useContext, useEffect, useState} from "react";
import {UserContext} from "../../App";
import {useNavigate} from "react-router-dom";

function PermissionAdd() {
    const navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);
    const {error, setError} = useState();
    const [inputs, setInputs] = useState({});
    const [formErrors, setFormErrors] = useState({});
    const [svaz, setSvaz] = useState([]);
    const [druh, setDruh] = useState([]);
    const [kategorie, setKategorie] = useState([]);
    const [typ, setTyp] = useState([]);

    const header = {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Request-Headers': 'session_token',
        'session_token': user.token
    }

    const headerPost = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Request-Headers': 'session_token',
        'session_token': user.token
    }

    const URL = "http://localhost:8080/api/v1/registry";
    const URL_PERMISSION = "http://localhost:8080/api/v1/permit";

    useEffect(() => {

        const getSvaz = async () => {
            try {
                const response = await fetch(URL + "/00001", {
                    headers: header
                });
                if (!response.ok) {
                    throw new Error(response.status)
                }
                let actualData = await response.json();
                setSvaz(actualData);
                setError(null);
            } catch (err) {
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
                setSvaz(null);
            }
        }

        const getDruh = async () => {
            try {
                const response = await fetch(URL + "/00002", {
                    headers: header
                });
                if (!response.ok) {
                    throw new Error(response.status)
                }
                let actualData = await response.json();
                setDruh(actualData);
                setError(null);
            } catch (err) {
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
                setDruh(null);
            }
        }

        const getKategorie = async () => {
            try {
                const response = await fetch(URL + "/00003", {
                    headers: header
                });
                if (!response.ok) {
                    throw new Error(response.status)
                }
                let actualData = await response.json();
                setKategorie(actualData);
                setError(null);
            } catch (err) {
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
                setKategorie(null);
            }
        }

        const getTyp = async () => {
            try {
                const response = await fetch(URL + "/00004", {
                    headers: header
                });
                if (!response.ok) {
                    throw new Error(response.status)
                }
                let actualData = await response.json();
                setTyp(actualData);
                setError(null);
            } catch (err) {
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
                setTyp(null);
            }
        }

        getSvaz();
        getDruh();
        getKategorie();
        getTyp();
    }, [])

    const errorCodes = [{"status": "400", "message": "Chyba validace"},
        {"status": "401", "message": "un authorized"},
        {"status": "409", "message": "Uzivatel jiz existuje"}];

    const setField = (field, value) => {
        setInputs({
            ...inputs,
            [field]: value
        });

        if (!!formErrors[field]) setFormErrors({
            ...formErrors,
            [field]: null
        })
    }

    const findFormErrors = () => {
        const {nazev, svaz, druh, kategorie, typ, mistniOrganizace, platnostOd, platnostDo} = inputs
        const newErrors = {}

        if (!nazev || nazev === '') newErrors.nazev = 'Nazev je povinny';
        if (!svaz || svaz === '') newErrors.svaz = 'Svaz je povinny';
        if (!druh || druh === '') newErrors.druh = 'Druh je povinny';
        if (!kategorie || kategorie === '') newErrors.kategorie = 'Kategorie je povinna';
        if (!typ || typ === '') newErrors.typ = 'Typ je povinny'
        if (!platnostOd || platnostOd === '') newErrors.platnostOd = 'Platnost Od je povinna'
        if (!platnostDo || platnostDo === '') newErrors.platnostDo = 'Platnost Do je povinna'
        if (!mistniOrganizace || mistniOrganizace === '') newErrors.mistniOrganizace = 'Misti organizace je povinna'

        const validFrom = new Date(platnostOd);
        const validTo = new Date(platnostDo);
        if (validFrom > validTo) newErrors.platnostDo = 'Planost do musi by vetsi nez platnost od'

        return newErrors;
    }

    const createPermission = async (data) => {
        try {
            const response = await fetch(URL_PERMISSION, {
                method: 'post',
                headers: headerPost,
                body: JSON.stringify(data)
            });
            if (!response.ok) {
                throw new Error(response.status)
            }
            navigate('/permission');
            setError(null);
        } catch (err) {
            const errorCode = errorCodes.find(e => e.status === err.message);
            if (errorCode.status === '401') {
                setUser({loggedIn: false, token: null});
                navigate('/');
            }
            setError(err.message);
        }
    }

    const handleOnSubmit = (event) => {
        event.preventDefault();
        console.log(JSON.stringify(inputs));
        const newError = findFormErrors();
        console.log(JSON.stringify(newError))
        if (Object.keys(newError).length > 0) {
            setFormErrors(newError);
        } else {
            setFormErrors({});
            const data = {
                name: inputs.nazev,
                svaz: inputs.svaz,
                druh: inputs.druh,
                kategorie: inputs.kategorie,
                typ: inputs.typ,
                platnostOd: inputs.platnostOd,
                platnostDo: inputs.platnostDo,
                mistniOrganizace: inputs.mistniOrganizace
            }
            console.log(JSON.stringify(data));
            createPermission(data);
        }
    }

    return (<Container>
        {error && (<Alert variant="danger">{error}</Alert>)}
        <Form onSubmit={handleOnSubmit}>
            <FormGroup>
                <Form.Label>Nazev</Form.Label>
                <Form.Control type='text'
                              onChange={e => setField('nazev', e.target.value)}
                              isInvalid={!!formErrors.nazev}/>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.nazev}
                </Form.Control.Feedback>
            </FormGroup>
            <FormGroup>
                <Form.Label>Svaz</Form.Label>
                <Form.Select onChange={e => setField('svaz', e.target.value)}
                             isInvalid={!!formErrors.nazev}>
                    <option selected>Vyber te</option>
                    {svaz && svaz.map((item) => (<option value={item.code}>{item.name}</option>))}
                </Form.Select>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.svaz}
                </Form.Control.Feedback>
            </FormGroup>
            <FormGroup>
                <Form.Label>Druh</Form.Label>
                <Form.Select onChange={e => setField('druh', e.target.value)}
                             isInvalid={!!formErrors.druh}>
                    <option selected>Vyber te</option>
                    {druh && druh.map((item) => (<option value={item.code}>{item.name}</option>))}
                </Form.Select>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.druh}
                </Form.Control.Feedback>
            </FormGroup>
            <FormGroup>
                <Form.Label>Kategorie</Form.Label>
                <Form.Select onChange={e => setField('kategorie', e.target.value)}
                             isInvalid={!!formErrors.kategorie}>
                    <option selected>Vyber te</option>
                    {kategorie && kategorie.map((item) => (<option value={item.code}>{item.name}</option>))}
                </Form.Select>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.kategorie}
                </Form.Control.Feedback>
            </FormGroup>
            <FormGroup>
                <Form.Label>Typ</Form.Label>
                <Form.Select onChange={e => setField('typ', e.target.value)}
                             isInvalid={!!formErrors.typ}>
                    <option selected>Vyber te</option>
                    {typ && typ.map((item) => (<option value={item.code}>{item.name}</option>))}
                </Form.Select>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.typ}
                </Form.Control.Feedback>
            </FormGroup>
            <FormGroup>
                <Form.Label>Mistni organizace</Form.Label>
                <Form.Control type='text'
                              onChange={e => setField('mistniOrganizace', e.target.value)}
                              isInvalid={!!formErrors.mistniOrganizace}/>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.mistniOrganizace}
                </Form.Control.Feedback>
            </FormGroup>
            <Form.Group>
                <Form.Label>Platnost od</Form.Label>
                <Form.Control type="date"
                              onChange={e => setField('platnostOd', e.target.value)}
                              isInvalid={!!formErrors.platnostOd}/>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.platnostOd}
                </Form.Control.Feedback>
            </Form.Group>
            <Form.Group>
                <Form.Label>Platnost Do</Form.Label>
                <Form.Control type="date"
                              onChange={e => setField('platnostDo', e.target.value)}
                              isInvalid={!!formErrors.platnostDo}/>
                <Form.Control.Feedback type='invalid'>
                    {formErrors.platnostDo}
                </Form.Control.Feedback>
            </Form.Group>
            <Button type="submit" className="btn btn-primary btn-dark">Pridat</Button>
        </Form>
    </Container>)
}

export default PermissionAdd;
