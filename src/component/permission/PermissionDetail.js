import {useNavigate, useParams} from "react-router-dom";
import {Alert, Button, Col, Container, Row, Table} from "react-bootstrap";
import React, {useContext, useEffect, useState} from "react";
import {UserContext} from "../../App";

function PermissionDetail() {
    const {uuid} = useParams();
    const navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);
    const [detail, setDetail] = useState(null);
    const {error, setError} = useState();
    const [attendences, setAttendences] = useState([])
    const header = {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Request-Headers': 'session_token',
        'session_token': user.token
    }

    const URL_PERMISSION_DETAIL = 'http://localhost:8080/api/v1/permit/';
    const URL_ATTENDENCES = 'http://localhost:8080/api/v1/attendance/';


    const errorCodes = [{"status": "400", "message": "Chyba validace"},
        {"status": "401", "message": "un authorized"},
        {"status": "409", "message": "Uzivatel jiz existuje"}];

    useEffect(() => {
        const loadPermissionDetail = async () => {
            await fetch(URL_PERMISSION_DETAIL + uuid, {
                headers: header
            }).then(res => {
                if (!res.ok)
                    throw new Error(res.status)
                return res.json();
            }).then(data => {
                    console.log(JSON.stringify(data))
                    console.log(typeof data);
                    setDetail(data);
                }
            ).catch(err => {
                console.log("ERROR: : " + err);
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
            });
        }

        const loadAttendence = async () => {
            await fetch(URL_ATTENDENCES + uuid, {
                headers: header
            }).then(res => {
                if (!res.ok)
                    throw new Error(res.status)
                return res.json();
            }).then(data => {
                    console.log(JSON.stringify(data))
                    console.log(typeof data);
                    setAttendences(data);
                }
            ).catch(err => {
                console.log("ERROR: : " + err);
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
            });
        }

        loadPermissionDetail();
        loadAttendence();
    }, [])

    const handleNewOnClick = () => {
        navigate("/attendance-add/" + uuid)
    }

    return (
        <Container>
            {error && (<Alert variant="danger">{error}</Alert>)}
            <Row>
                <Col><h1>{detail && detail.name}</h1></Col>
            </Row>
            <Row>
                <Col>{detail && detail.svaz}</Col>
                <Col>{detail && detail.druh}</Col>
                <Col>{detail && detail.kategorie}</Col>
                <Col>{detail && detail.typ}</Col>
            </Row>
            <Row>
                <Col>{detail && detail.mistniOrganizace}</Col>
            </Row>
            <Row>
                <Col>Platnost</Col>
                <Col>{detail && detail.platnostOd}</Col>
                <Col>{detail && detail.platnostDo}</Col>
            </Row>
            <Row>
                <Col>Stav</Col>
                <Col>{detail && detail.stav}</Col>
            </Row>
            <Row>
                <Col><Button className="btn btn-primary btn-dark" onClick={handleNewOnClick}>Pridat</Button></Col>
            </Row>
            <Row>
                <Col>
                    <Table variant="dark">
                        <thead>
                        <tr>
                            <th>DATUM</th>
                            <th>REVIR</th>
                            <th>PODREVIR</th>
                            <th>DRUH</th>
                            <th>MNOZSTVI</th>
                            <th>VAHA</th>
                            <th>DELKA</th>
                        </tr>
                        </thead>
                        <tbody>
                        {attendences && attendences.map((item) => (
                            <tr key={item.uuid}>
                                    <td>{item.datum}</td>
                                    <td>{item.revir} - {item.revirName}</td>
                                    <td>{item.podrevir} - {item.podrevirName}</td>
                                    <td>{item.druh}</td>
                                    <td>{item.mnozstvi}</td>
                                    <td>{item.vaha}</td>
                                    <td>{item.delka}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    )
}

export default PermissionDetail;

