import {Alert, Button, Col, Container, Row, Table} from "react-bootstrap";
import {Link, useNavigate} from "react-router-dom";
import React, {useContext, useEffect, useState} from "react";
import PermissionAPI from "../service/PermissionAPI";
import {UserContext} from "../../App";

import deleteImage from "../../delete.png";
import refreshImage from "../../refresh.png";
import pencilImage from "../../pencil.png";
import timeMachineImage from "../../timeMachione.png"


function PermissionList() {
    const {user, setUser} = useContext(UserContext);
    const [error, setError] = useState();
    const navigate = useNavigate();
    const [permissions, setPermissions] = useState([]);

    const errorCodes = [{"status": "401", "message": "Unauthorized"},
        {"status": "409", "message": "Chybne heslo"}];

    const handleNewOnClick = () => {
        navigate("/permission-add")
    }


    const remove = (uuid) => {
        console.log("Delete");
    }

    const handleDetail = (uuid) => {
        console.log(uuid);
    }
    useEffect(() => {
        async function loadPermission() {
            const response = PermissionAPI.getAll(user.token);
            response.then(res => {
                console.log("RESPONSE: " + JSON.stringify(res));
                setPermissions(res);
            }).catch(err => {
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(errorCode.message);
            })
        }

        loadPermission()
    }, [])

    return (
        <Container>
            {error && (<Alert variant="danger">{error}</Alert>)}
            <Row>
                <Col>
                    <Button className="btn btn-primary btn-dark" onClick={handleNewOnClick}>Nova</Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Table striped bordered hover variant="dark">
                        <thead>
                        <tr>
                            <th>Nazev</th>
                            <th>svaz</th>
                            <th>druh</th>
                            <th>kategorie</th>
                            <th>typ</th>
                            <th>planost od</th>
                            <th>planost do</th>
                            <th>stav</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {permissions && permissions.map((item) => (
                            <tr key={item.uuid}>
                                <td><Link to={`/permission/${item.uuid}`}> {item.name} </Link></td>
                                <td>{item.svaz}</td>
                                <td>{item.druh}</td>
                                <td>{item.kategorie}</td>
                                <td>{item.typ}</td>
                                <td>{item.platnostOd}</td>
                                <td>{item.platnostDo}</td>
                                <td>{item.stav}</td>
                                <td>
                                    <img src={deleteImage} alt="delete" onClick={() => remove(item.uuid)}/>
                                    <img src={pencilImage}/>
                                    <img src={timeMachineImage}/>
                                    <img src={refreshImage}/>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    )
}

export default PermissionList;
