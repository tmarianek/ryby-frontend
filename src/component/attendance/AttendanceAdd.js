import {Alert, Button, Col, Container, Form, FormGroup, Row} from "react-bootstrap";
import {useNavigate, useParams} from "react-router-dom";
import React, {useContext, useEffect, useState} from "react";
import {UserContext} from "../../App";

function AttendanceAdd() {
    const {uuid} = useParams();
    const navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);
    const {error, setError} = useState();
    const [inputs, setInputs] = useState({});
    const [formErrors, setFormErrors] = useState({});
    const [regRevir, setRegRevir] = useState([]);
    const [regPodrevir, setRegPodrevir] = useState([]);
    const [druh, setDruh] = useState([]);

    // TODO: URL bych dal nekde globalne, do nejakeho config souboru. Budes chctit menit URL_BASE pres environment (koukni na dotenv + dokumentaci create-react-app, maji to vyresene)
    const URL_ATTENDANCE = 'http://localhost:8080/api/v1/attendance';
    const URL_DISTRICT = 'http://localhost:8080/api/v1/district';
    // TODO: Headers bych taku dal nekde globalne, nejlepe kdyby se daly nastavit globalne do toho fetch, React Query to asi poresi.
    const header = {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Access-Control-Request-Headers': 'session_token',
        'session_token': user.token
    }

    const headerPost = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Request-Headers': 'session_token',
        'session_token': user.token
    }

    const URL = "http://localhost:8080/api/v1/registry";

    // TODO: error taky bych to hodil někde na samostatne misto
    // TODO: koukni na knihovny na překlady např. react-intl nebo i18next
    const errorCodes = [{"status": "400", "message": "Chyba validace"},
        {"status": "401", "message": "un authorized"},
        {"status": "409", "message": "Uzivatel jiz existuje"}];

    // TODO: Na zacatek je tohle na formulare v pohode, jen na vetsich aplikacich to bude trochu mess a problem to udrzovat.
    //   Formulare se resi v kazde apce a je to furt dokola, jsou na to slusne knihovny.
    //   Koukni se na Formik a React Hook Form na hadlovani state formularu a na yup ohledně validací formulářů.
    const setField = (field, value) => {
        if (value === 'Vyber te') {
            value = null;
        }
        setInputs({
            ...inputs,
            [field]: value
        });

        if (!!formErrors[field]) setFormErrors({
            ...formErrors,
            [field]: null
        })
    }

    const findFormErrors = () => {
        const {datum, revir, podrevir, druh, mnozstvi, vaha, delka} = inputs
        const newErrors = {}

        // TODO: tady ty validace ti vyresi yup
        if (!datum || datum === '') newErrors.datum = 'Datum je povinne';
        if (!revir || revir === '') newErrors.revir = 'Revir je povinny';
        if (regPodrevir.length > 0 && (!podrevir || podrevir === '')) newErrors.podrevir = 'Podrevir je povinny';
        if (druh && druh !== '') {
            if (!mnozstvi || mnozstvi === '') newErrors.mnozstvi = 'Mnozstvi je povinne';
            if (!vaha || vaha === '') newErrors.vaha = 'Vaha je povinne';
            if (!delka || delka === '') newErrors.delka = 'Delka je povinne';
        }
        return newErrors;
    }

    const createAttendance = async (data) => {

        try {
            const response = await fetch(URL_ATTENDANCE + "/" + uuid, {
                method: 'post',
                headers: headerPost,
                body: JSON.stringify(data)
            });
            if (!response.ok) {
                throw new Error(response.status)
            }
            navigate('/permission/' + uuid);
            setError(null);
        } catch (err) {
            const errorCode = errorCodes.find(e => e.status === err.message);
            if (errorCode.status === '401') {
                setUser({loggedIn: false, token: null});
                navigate('/');
            }
            setError(err.message);
        }
    }

    const handleOnSubmit = (event) => {
        event.preventDefault();
        const newError = findFormErrors();
        if (Object.keys(newError).length > 0) {
            setFormErrors(newError);
        } else {
            setFormErrors({});
            const data = {
                datum: inputs.datum,
                revir: inputs.revir,
                podrevir: inputs.podrevir,
                druh: inputs.druh,
                mnozstvi: inputs.mnozstvi,
                vaha: inputs.vaha,
                delka: inputs.delka
            }
            console.log(JSON.stringify(data));
            createAttendance(data);
        }
    }


    const getPodrevir = async (value) => {
        // TODO: V pohodě řešení na začátek ale trochu se upíšeš a nemáš tady pořešený loading state, abys vykreslil uživateli nějaký feedback
        //   Je to taky obecny problem, ktery se resi vsude. Koukni na knihovnu React Query, mají to vyresene dobře, včetně peknych feature jako opakovani requestu, pokud kvuli siti neco neprojde atp.
        await fetch(URL_DISTRICT + '/' + value, {
            headers: header
        }).then(res => {
            if (!res.ok)
                throw new Error(res.status)
            return res.json();
        }).then(data => {
                console.log('DATA DISTRICT: ' + JSON.stringify(data))
                console.log(typeof data);
                setRegPodrevir(data);
            }
        ).catch(err => {
            console.log("ERROR: : " + err);
            const errorCode = errorCodes.find(e => e.status === err.message);
            if (errorCode.status === '401') {
                setUser({loggedIn: false, token: null});
                navigate('/');
            }
            setError(err.message);
        });

        setField('revir', value)
    }


    useEffect(() => {
        const loadDistrict = async () => {
            await fetch(URL_DISTRICT, {
                headers: header
            }).then(res => {
                if (!res.ok)
                    throw new Error(res.status)
                return res.json();
            }).then(data => {
                    console.log('DATA DISTRICT: ' + JSON.stringify(data))
                    console.log(typeof data);
                    setRegRevir(data);
                }
            ).catch(err => {
                console.log("ERROR: : " + err);
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
            });
        }

        const getDruh = async () => {
            try {
                const response = await fetch(URL + "/00006", {
                    headers: header
                });
                if (!response.ok) {
                    throw new Error(response.status)
                }
                let actualData = await response.json();
                setDruh(actualData);
                setError(null);
            } catch (err) {
                const errorCode = errorCodes.find(e => e.status === err.message);
                if (errorCode.status === '401') {
                    setUser({loggedIn: false, token: null});
                    navigate('/');
                }
                setError(err.message);
                setDruh(null);
            }
        }


        loadDistrict();
        getDruh();

    }, []);

    // TODO: Formatujes tak trochu podivne, koukni se na Prettier, je to klihovna i plugin do IDE, ktery zaruci, ze vsichni v typu formatuji stejne.
    return (<Container>
        {error && (<Alert variant="danger">{error}</Alert>)}
        <Form onSubmit={handleOnSubmit}>
            <Row>
                <Col>
                    <FormGroup>
                        <Form.Label>Datum</Form.Label>
                        <Form.Control type='date'
                                      onChange={e => setField('datum', e.target.value)}
                                      isInvalid={!!formErrors.datum}/>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.datum}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Form.Label>Revir</Form.Label>
                        <Form.Select onChange={e => getPodrevir(e.target.value)}
                                     isInvalid={!!formErrors.revir}>
                            <option selected>Vyber te</option>
                            {regRevir && regRevir.map((item) => (
                                <option value={item.number}>{item.number} - {item.name}</option>))}
                        </Form.Select>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.revir}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Form.Label>Podrevir</Form.Label>
                        <Form.Select onChange={e => setField('podrevir', e.target.value)}
                                     isInvalid={!!formErrors.podrevir}>
                            <option selected>Vyber te</option>
                            {regPodrevir && regPodrevir.map((item) => (
                                <option value={item.number}>{item.number} - {item.name}</option>))}
                        </Form.Select>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.podrevir}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col>
                    <FormGroup>
                        <Form.Label>Druh</Form.Label>
                        <Form.Select onChange={e => setField('druh', e.target.value)}
                                     isInvalid={!!formErrors.druh}>
                            <option value={null} selected>Vyber te</option>
                            {druh && druh.map((item) => (
                                <option value={item.code}>{item.name}</option>))}
                        </Form.Select>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.druh}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Form.Label>Mnozstvi</Form.Label>
                        <Form.Control type='number'
                                      onChange={e => setField('mnozstvi', e.target.value)}
                                      isInvalid={!!formErrors.mnozstvi}/>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.mnozstvi}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Form.Label>Vaha</Form.Label>
                        <Form.Control type='text'
                                      onChange={e => setField('vaha', e.target.value)}
                                      isInvalid={!!formErrors.vaha}/>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.vaha}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Form.Label>Delka</Form.Label>
                        <Form.Control type='text'
                                      onChange={e => setField('delka', e.target.value)}
                                      isInvalid={!!formErrors.delka}/>
                        <Form.Control.Feedback type='invalid'>
                            {formErrors.delka}
                        </Form.Control.Feedback>
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col> <Button type={"submit"}>Pridat</Button> </Col>
            </Row>
        </Form>
    </Container>)
}

export default AttendanceAdd;
