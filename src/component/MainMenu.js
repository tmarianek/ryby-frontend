import {Link, NavLink, useNavigate} from "react-router-dom";
import {useContext} from "react";
import {UserContext} from "../App";
import {Container, Nav, Navbar} from "react-bootstrap";
import UserAPI from "./service/UserAPI";

function MainMenu() {
    const {user, setUser} = useContext(UserContext)
    const navigate = useNavigate();

    const handleLogout = () => {
        setUser({loggedIn: false, token: null});
        navigate('/home');
    };


    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Nav>
                    <Nav.Link as={Link} to="/home">Home</Nav.Link>
                    <Nav.Link as={Link} to="/permission">Povolenky</Nav.Link>
                    <Nav.Link as={Link} to="/attendance">Dochazka</Nav.Link>
                    {user.loggedIn && (<Nav.Link as={Link} to="/" onClick={handleLogout}>odhlasit </Nav.Link>)}
                </Nav>
            </Container>
        </Navbar>
    )
}

// TODO: Exporty jsou občas diskutabilní ale obecně je lepší způsob používat named exporty kde to jde, místo default export
//   tzn. export function MainMenu() {....
//   import { MainMenu } from '...
export default MainMenu;
